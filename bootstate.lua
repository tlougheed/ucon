local initializeDisplay = function()
	DISPLAY_WIDTH = 800
	DISPLAY_HEIGHT = 600
end

local initializeMouse = function()
	MOUSE.pos = Vector()
	MOUSE.delta = Vector()	
	MOUSE.lastTimeClicked = nil
	MOUSE.doubleClickThreshold = 0.3
	MOUSE.checkForDoubleClick = function()
		if not MOUSE.lastTimeClicked then 
			MOUSE.lastTimeClicked = love.timer.getTime() 
			return false
		else					
			local timeBetweenClicks = love.timer.getTime() - MOUSE.lastTimeClicked			
			if timeBetweenClicks < MOUSE.doubleClickThreshold then
				MOUSE.lastTimeClicked = love.timer.getTime()
				return true
			else
				MOUSE.lastTimeClicked = love.timer.getTime()
				return false
			end
		end
	end
end

function bootState:update(dt)		
	initializeDisplay()		
	initializeMouse()
	startUp()
	Gamestate.switch(menuState)
end