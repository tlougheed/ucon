function startUp()

	t = 0 --for timing purposes

	debug_flag = false

	map_flag = false

	game_condition = "in progress"

	love.math.setRandomSeed(os.time())

	mouse_time = 0
	double_click_interval = 0.5 		--seconds between double clicks

	love.graphics.setFont(defaultFont)

	Player:new()
	Player:init()

	TileMatrix:new()
	TileMatrix:init()

	Blob:new()
	Blob:init()

	--set up lab
	Toolbox:new()
	Toolbox:init()

	Workbench:new()
	Workbench:init()

	missileArray = {}

	love.graphics.setColor(WHITE)

end