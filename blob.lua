Blob = {}

function Blob:new(o)
	local o=o or {}
	setmetatable(o, self)
	self.__index = self
	return o
end

function Blob:init()
	
	self.fortifiedColor = {0,240,0}
	self.filledColor = {0,160,0}
	self.tileArray = {}

	self.harm_flag = false

	self.coverage = 0

	self.starting_coverage = START_COVERAGE --expressed as a percentage (self.coverage/300)

	self.saturation_point = 100

	self.growth_chance = GROWTH_CHANCE

	self.neural_threshold = 10 --how much of a tile be occupied before the blob can transmit impulses through the block

	self.wobble = 6 --used for animating the blob

	local found_flag = false

	io.write("Initializing Nanogoo...")

	repeat 
	self:update()
	until self.coverage/300 > self.starting_coverage

	print("done.")

	--self.origin = {x = love.math.random(1, TileMatrix.width), y = love.math.random(1, TileMatrix.height)}

	--TileMatrix[self.origin.x][self.origin.y].state = 1

end

function Blob:draw()
	balls = {}
	for _,b in ipairs(self.tileArray) do
		local offset = (32 - (28*(b.state/100)))/2
		if b.state > 0 and b.state <= 50 then
			love.graphics.setColor(self.filledColor)
		elseif b.state > 50 then
			love.graphics.setColor(self.fortifiedColor)
		end
		table.insert(balls, {
				math.sin(Blob.wobble*t) + (b.position.x-1)*TileMatrix.tile_size[1]+offset+TileMatrix.topLeft[1] + 16, 
				math.cos(Blob.wobble*t) -((b.position.y-1)*(TileMatrix.tile_size[2])+offset+TileMatrix.topLeft[2]) + love.window.getHeight() - 16
				})
	end

	love.graphics.setColor({0,200,0})
	if #balls > 0 then
		metaballs:send('balls', unpack(balls))
	end
	self.harm_flag = false
end

--called when a bomb hits
function Blob:hit(hit_pos, total_effect, partArray)
	--print("hitting") --DEBUG
	for _,b in ipairs(self.tileArray) do
		local offset = (32 - (28*(b.state/100)))/2
		local b_pos = {(b.position.x-1)*TileMatrix.tile_size[1]+offset+TileMatrix.topLeft[1], (b.position.y-1)*TileMatrix.tile_size[2]+offset+TileMatrix.topLeft[2]}
		if DistanceBetween(b_pos, hit_pos) < 28 then
			self.harm_flag = true
			if debug_flag or total_effect > 50 then
				b.state = 0
			else
				b.state = b.state - total_effect*2
			end
			if b.state < 0 then 
				b.state = 0
			elseif b.state > 100 then
				self.growth_chance = self.growth_chance - math.floor(total_effect/10)
				b.state = 100
			end

			--play the explosion sound(s)
			for _,p in ipairs(partArray) do
				sndComp[p.ID]:play()
			end
			break
		end
	end

	--adapt the blob to the attack (eventually this will be localized instead of global, and it will take time to propagate the adaptations through the blob)
	if self.harm_flag then
		if #partArray == 1 then
			local p = partArray[1]
			p.show_effect = true
			if p.effectiveness > -95 then
				p.times_used = p.times_used + 1*DIFFICULTY_FACTOR
				--if debug_flag then print(p.times_used) end
				p.effectiveness = p.effectiveness - p.times_used
				if p.effectiveness < -100 then p.effectiveness = -100 end
			end

		elseif #partArray == 2 then
			local p1 = partArray[1]
			local p2 = partArray[2]
			
			p1.times_used = p1.times_used + 0.3*DIFFICULTY_FACTOR
			--if debug_flag then print(p1.times_used) end
			p1.effectiveness = p1.effectiveness - p1.times_used
			if p1.effectiveness < -100 then p1.effectiveness = -100 end

			p2.times_used = p2.times_used + 0.5*DIFFICULTY_FACTOR
			--if debug_flag then print(p2.times_used) end
			p2.effectiveness = p2.effectiveness - p2.times_used
			if p2.effectiveness < -100 then p2.effectiveness = -100 end
			
		elseif #partArray == 3 then
			local p1 = partArray[1]
			local p2 = partArray[2]
			local p3 = partArray[3]
			
			p1.times_used = p1.times_used + 0.1*DIFFICULTY_FACTOR
			--if debug_flag then print(p1.times_used) end
			p1.effectiveness = p1.effectiveness - p1.times_used
			if p1.effectiveness < -100 then p1.effectiveness = -100 end
			
			p2.times_used = p2.times_used + 0.1*DIFFICULTY_FACTOR
			--if debug_flag then print(p2.times_used) end
			p2.effectiveness = p2.effectiveness - p2.times_used
			if p2.effectiveness < -100 then p2.effectiveness = -100 end
			
			p3.times_used = p3.times_used + 0.3*DIFFICULTY_FACTOR
			--if debug_flag then print(p3.times_used) end
			p3.effectiveness = p3.effectiveness - p3.times_used
			if p3.effectiveness < -100 then p3.effectiveness = -100 end
			
		end
	end
end

function Blob:update(dt)

	self.coverage = 0
	self.tileArray = {}
	for _,c in ipairs(TileMatrix.matrix) do
		for _,t in ipairs(c) do
			if t.state > 0 then
				self.coverage = self.coverage + t.state
				table.insert(self.tileArray, t)
			end
		end
	end

	--local chance_of_growth = love.math.random(0,8)

	--grow the blob, and TODO: send neural impulses through the blob

	--check neighbour blocks starting from the right
	for _,b in ipairs(self.tileArray) do
		--local Neighbours = {b.right, b.right.below, b.below, b.below.left, b.left, b.left.above, b.above, b.above.right}
		local Neighbours = {b.right, b.below, b.left, b.above}
		if b.state < self.saturation_point then b.state = b.state + 1 end
		if b.state > love.math.random(40,60) then
			for _,n in ipairs(Neighbours) do
				local chance_of_growth = love.math.random(0, self.growth_chance)
				--print(n.position.x..", "..n.position.y) --DEBUG
				if b.state > 50 and chance_of_growth == 0 then
					n.state = n.state + 1
					sndBlobGrow:play()
					break
				elseif b.state > 50 and n.state < self.saturation_point and chance_of_growth == 0 then
					n.state = n.state + 1
					sndBlobGrow:play()
					break
				end
			end
			-- for _,n in ipairs(Neighbours) do
			-- 	if n.state > self.neural_threshold then
			-- 		--TODO: transmit impulse, if one is waiting
			-- 	end
			-- end
		end

		--cap the block's fill value
		if b.state > self.saturation_point then b.state = self.saturation_point end
	end

end