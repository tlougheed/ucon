local menuType = nil

function menuState:update(dt)
	if RUNNING and menuType == 'start' then
		menuType = 'in game'
	else
		if not RUNNING then menuType = 'start' end
		if RUNNING == false and menuType == 'in game' then love.event.push('quit') end
	end
end

function menuState:enter()
	--RUNNING = true
end

function menuState:leave()
	musTitle:pause()
end

function menuState:draw()		
	if menuType == 'start' then
		
		local title_width = titleFont:getWidth('U.C.O.N')
		love.graphics.setColor(LIGHT_SLATE_BLUE)
		love.graphics.setFont(titleFont)
		love.graphics.print('U.C.O.N.', DISPLAY_WIDTH/2 - title_width/2, DISPLAY_HEIGHT/2 - DISPLAY_HEIGHT/3)

		love.graphics.setFont(menuFont)
		local f_width = menuFont:getWidth('Un-conventional Ordinance Neutralizer')
		love.graphics.print('Un-conventional Ordinance Neutralizer', DISPLAY_WIDTH/2 - f_width/2, DISPLAY_HEIGHT/2 - DISPLAY_HEIGHT/10)

		love.graphics.setFont(defaultFont)
		love.graphics.setColor(GAINSBORO)
		local n_width = defaultFont:getWidth('A game by Ted Lougheed for Ludum Dare 32')
		love.graphics.print('A game by Ted Lougheed for Ludum Dare 32', DISPLAY_WIDTH/2 - n_width/2, DISPLAY_HEIGHT/2)

		love.graphics.setFont(bigFont)
		local font_width = bigFont:getWidth('START')

		if MOUSE.pos.x >= DISPLAY_WIDTH/2 - font_width/2
		and MOUSE.pos.x <= DISPLAY_WIDTH/2 + font_width/2
		and MOUSE.pos.y >= DISPLAY_HEIGHT/2 + DISPLAY_HEIGHT/4
		and MOUSE.pos.y <= DISPLAY_HEIGHT/2 + DISPLAY_HEIGHT/4 + 36
		then
			love.graphics.setColor(BLUE)
		else
			love.graphics.setColor(GAINSBORO)
		end
		
		love.graphics.print('START', DISPLAY_WIDTH/2 - font_width/2, DISPLAY_HEIGHT/2 + DISPLAY_HEIGHT/4)

	elseif menuType == 'in game' then
		musTitle:resume()
		love.graphics.setFont(menuFont)
		love.graphics.setColor(LIGHT_SLATE_BLUE)
		local font_width = menuFont:getWidth('Esc: Resume')
		love.graphics.print('Esc: Resume', DISPLAY_WIDTH/2 - font_width/2, 92)
		local font_width2 = menuFont:getWidth('Q: Quit Game')
		love.graphics.print('Q: Quit', DISPLAY_WIDTH/2 - font_width2/2, 128)

		instructionString = "Build unusual weapons to destroy the Nanogoo. Double-click a part in the toolbar on the left to add it to the highlighted slot in the workbench. Click on the map to launch the weapon you've designed at part of the Goo.	As you use your weapons, the Nanogoo adapts to your attacks and they become less effective.	Firing a single component at the Goo will reveal its effectiveness rating (red text) - use this information to construct better weapons. The Goo is less able to adapt to combinations of parts. Be careful - some components may actually heal the Goo and make it harder to defeat."

		love.graphics.setFont(instructionFont)
		love.graphics.setColor(SILVER)
		love.graphics.printf(instructionString, DISPLAY_WIDTH/2 - DISPLAY_WIDTH/3, 196, DISPLAY_WIDTH/1.5, 'left')
				
	end	
end
function menuState:keypressed(key)
	KEYS[key] = true
end

function menuState:keyreleased(key)
	KEYS[key] = false
	if menuType == 'start' then 
		if key == 'return' then
			Gamestate.switch(gameState)
		elseif key == 'q' or key == 'escape' then
			RUNNING = false
			--Gamestate.switch(gameState)
		end
	elseif menuType == 'in game' then
		if key == 'escape' or key == 'f1' then
			Gamestate.switch(gameState)
		elseif key == 'q' then
			RUNNING = false
			Gamestate.switch(gameState)					
		end		
	end
end

function menuState:mousepressed(x, y, button)
	MOUSE[button] = true
	
	if mouse_time <= double_click_interval and (button == 'l' or button == 'r') then
		double_click = true
		mouse_time = 0
	else
		double_click = false
		mouse_time = 0
	end

	local font_width = bigFont:getWidth('START')

	if MOUSE.pos.x >= DISPLAY_WIDTH/2 - font_width/2
	and MOUSE.pos.x <= DISPLAY_WIDTH/2 + font_width/2
	and MOUSE.pos.y >= DISPLAY_HEIGHT/2 + DISPLAY_HEIGHT/4
	and MOUSE.pos.y <= DISPLAY_HEIGHT/2 + DISPLAY_HEIGHT/4 + 36
	and button == 'l'
	then
		Gamestate.switch(gameState)
	end

end

function menuState:mousemoved(x, y, dx, dy)	
	MOUSE.delta.x = dx
	MOUSE.delta.y = dy	 
	MOUSE.pos.x, MOUSE.pos.y = x, y
end