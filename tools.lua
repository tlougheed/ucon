---------------------------------------------------------------------   DistanceBetween parameters can be any combination of vectors and tables
function DistanceBetween(pointA, pointB)
	if Vector.isvector(pointA) and Vector.isvector(pointB) then 					--if both points are vectors
		local dx = pointB.x - pointA.x
    	local dy = pointB.y - pointA.y
    	return math.abs(math.sqrt(dx ^ 2 + dy ^ 2))
  	else 																			--both points are not vectors
  		local tempA = pointA
  		local tempB = pointB
  		if Vector.isvector(pointA) then 												--if pointA is a vector convert to table
  			tempA = {pointA.x, pointA.y}
  		end
		if Vector.isvector(pointB) then 												--do the same for pointB
  			tempB = {pointB.x, pointB.y}
  		end  		
    	local dx = tempB[1] - tempA[1]
		local dy = tempB[2] - tempA[2]
		return math.abs(math.sqrt(dx ^ 2 + dy ^ 2))
    end 
end

-------------------------------------------------------------------------
function AngleBetween(pointA, pointB) --this will calculate the atan2 of the line between points (without having to manually calculate the rise and run)

	if Vector.isvector(pointA) and Vector.isvector(pointB) then 					--if both points are vectors
		local dx = pointB.x - pointA.x
    	local dy = pointB.y - pointA.y
    	return math.atan2(dy, dx)
  	else 																			--both points are not vectors
  		local tempA = pointA
  		local tempB = pointB
  		if Vector.isvector(pointA) then 												--if pointA is a vector convert to table
  			tempA = {pointA.x, pointA.y}
  		end
		if Vector.isvector(pointB) then 												--do the same for pointB
  			tempB = {pointB.x, pointB.y}
  		end  		
    	local dx = tempB[1] - tempA[1]
		local dy = tempB[2] - tempA[2]
		return math.atan2(dy, dx)
    end 
end

-------------------------------------------------------------------------------
function AngleNorm(angle)
    --returns an angle between 0 and 2*pi
    return (angle) % (math.pi*2)
end

--=============================================================
--RANDOMIZATION FUNCTIONS

--pick a random item from a list
--currently this function assumes that each item has an equal chance of being picked, but at some point I will add the ability to set probabilities
function Pick(...)
	local options = {...}
	--pick a random index and return the corresponding datum
	local choice = love.math.random(1, #options)
	return options[choice]
end

------------------------------------------------------------------------LinesIntersect paramters must be tables with 2 points each, the points themselves 
-----------------------------------------------------------------------  can be any combination of tables and vectors

function LinesIntersect(passedLineA, passedLineB)
	local x, y = 1, 2
	local convertedLineA = nil 
	local convertedLineB = nil 

	local tempPointA = passedLineA[1]
	local tempPointB = passedLineA[2]
	local tempPointC = passedLineB[1]
	local tempPointD = passedLineB[2]	

	if Vector.isvector(passedLineA[1]) then tempPointA = {passedLineA[1].x, passedLineA[1].y} end
	if Vector.isvector(passedLineA[2]) then tempPointB = {passedLineA[2].x, passedLineA[2].y} end

	if Vector.isvector(passedLineB[1]) then tempPointC = {passedLineB[1].x, passedLineB[1].y} end
	if Vector.isvector(passedLineB[2]) then tempPointD = {passedLineB[2].x, passedLineB[2].y} end

	convertedLineA = {tempPointA, tempPointB}
	convertedLineB = {tempPointC, tempPointD}
	
	local isPointOnLine = function(lineSegment, point)
		local adjustedLine = {{ 0, 0 }, { lineSegment[2][x] - lineSegment[1][x], lineSegment[2][y] - lineSegment[1][y] }}
		local adjustedPoint = { point[x] - lineSegment[1][x], point[y] - lineSegment[1][y] }		
		--get cross product of adjustedLine[2] and adjustedPoint
		local a = adjustedLine[2]
		local b = adjustedPoint		
		result = a[x] * b[y] - b[x] * a[y]
		return math.abs(result) < 0.000001		
	end
	
	local isPointRightOfLine = function(lineSegment, point)
		local adjustedLine = {{ 0, 0 }, { lineSegment[2][x] - lineSegment[1][x], lineSegment[2][y] - lineSegment[1][y] }}
		local adjustedPoint = { point[x] - lineSegment[1][x], point[y] - lineSegment[1][y] }		
		--get cross product of adjustedLine[2] and adjustedPoint
		local a = adjustedLine[2]
		local b = adjustedPoint		
		result = a[x] * b[y] - b[x] * a[y]
		return result < 0
	end
	
	local lineSegmentTouchesOrCrossesLine = function(lineA, lineB)
		local condensed = not isPointRightOfLine(lineA, lineB[1]) ~= not isPointRightOfLine(lineA, lineB[2])
		return isPointOnLine(lineA, lineB[1]) or isPointOnLine(lineA, lineB[2]) or condensed
	end
	
	local doLineBoundsIntersect = function(a, b)		
		local leftA = math.min(a[1][x], a[2][x])
		local leftB = math.min(b[1][x], b[2][x])
		local rightA = math.max(a[1][x], a[2][x])
		local rightB = math.max(b[1][x], b[2][x])
		local topA = math.min(a[1][y], a[2][y])
		local topB = math.min(b[1][y], b[2][y])
		local botA = math.max(a[1][y], a[2][y])
		local botB = math.max(b[1][y], b[2][y])
		return (leftA <= rightB and 
			   rightA >= leftB and
			   topA <= botB and
			   botA >= botB) 
			or
			   (leftB <= rightA and 
			   rightB >= leftA and
			   topB <= botA and
			   botB >= botA)	
	end

	return doLineBoundsIntersect(convertedLineA, convertedLineB) and 
		   lineSegmentTouchesOrCrossesLine(convertedLineA, convertedLineB) and
		   lineSegmentTouchesOrCrossesLine(convertedLineB, convertedLineA)
end

---------------------------------------------------------------------- MiddleOfLine parameters can be any combination of tables and vectors
function MiddleOfLine(lineStartPoint, lineEndPoint)
	if Vector.isvector(lineStartPoint) and Vector.isvector(lineEndPoint) then
    	local midPoint = Vector((lineStartPoint.x + lineEndPoint.x) / 2, (lineStartPoint.y + lineEndPoint.y) / 2)
    	return midPoint
    else
    	local tempA = lineStartPoint
  		local tempB = lineEndPoint
  		if Vector.isvector(lineStartPoint) then 												--if lineStartPoint is a vector convert to table
  			tempA = {lineStartPoint.x, lineStartPoint.y}
  		end
		if Vector.isvector(lineEndPoint) then 												--do the same for lineEndPoint
  			tempB = {lineEndPoint.x, lineEndPoint.y}
  		end  		

    	local midPoint = {(tempA[1] + tempB[1]) / 2, (tempA[2] + tempB[2]) / 2}
    	return midPoint
  	end
end
-------------------------------------------------------------------- Rotate around points can be any combination of tables and vectors,
------------------------------------------------------------------     angle must be in radians
function RotateAround(pointA, pointB, angle)   
  if Vector.isvector(pointA) and Vector.isvector(pointB) then
    local rx = pointA.x- pointB.x
    local ry = pointA.y - pointB.y
    local newx = rx * math.cos(angle) - ry * math.sin(angle)
    local newy = rx * math.sin(angle) + ry * math.cos(angle)
    local finalx = newx + pointB.x
    local finaly = newy + pointB.y
    return Vector(finalx, finaly)
  else  
  	local tempA = pointA
  	local tempB = pointB
  	if Vector.isvector(pointA) then 												--if pointA is a vector convert to table
  		tempA = {pointA.x, pointA.y}
  	end
	if Vector.isvector(pointB) then 												--do the same for pointB
  		tempB = {pointB.x, pointB.y}
  	end  		
    local rx = tempA[1] - tempB[1]
    local ry = tempA[2] - tempB[2]
    local newx = rx * math.cos(angle) - ry * math.sin(angle)
    local newy = rx * math.sin(angle) + ry * math.cos(angle)
    local finalx = newx + tempB[1]
    local finaly = newy + tempB[2]
  	return {finalx, finaly}
  end
end

-----------------------------------------------------------------------------------------------------------------------------------
--[[ 																ValidScreenResolutions takes a target resolution 
																	(a table with .width and .height members) and returns 
																	a list of resolutions that are supported by the users 
																	video card, and match the target resolutions ratio, but 
																	do not exceed its dimensions			    	 		   ]]--
-----------------------------------------------------------------------------------------------------------------------------------
function ValidScreenResolutions(targetResolution)  
	local validFullscreenModes = love.window.getFullscreenModes()	
	local validResolutions = {}
	for _, mode in pairs(validFullscreenModes) do 
  		local modeRatio = mode.height / mode.width  		   		
  		local validSize = (targetResolution.width >= mode.width and targetResolution.height >= mode.height)
  		local targetRatio = targetResolution.height / targetResolution.width
  		if modeRatio == targetRatio and validSize then	
  		 	table.insert(validResolutions, mode)   			
  		end  	
  	end
  	return validResolutions
end