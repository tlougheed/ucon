--=============================================================
Background = {}

function Background:new(o)
	local o=o or {}
	setmetatable(o, self)
	self.__index = self
	return o
end

function Background:init()

	self.canvas = love.graphics.newCanvas(640,480)
	self.tile_canvas = love.graphics.newCanvas(640,480)

	love.graphics.setColor(WHITE)
	love.graphics.setCanvas(self.canvas)
		love.graphics.draw(mapUntiled)
	love.graphics.setCanvas()

	love.graphics.setColor(WHITE)
	love.graphics.setCanvas(self.tile_canvas)
		love.graphics.draw(mapTiled)
	love.graphics.setCanvas()

end

function Background:draw(xpos,ypos)
	love.graphics.draw(self.canvas, xpos, ypos)
end

--================
--create background
Background:new()
Background:init()

--==============================================================
Tile = {}

function Tile:new(o)
	local o=o or {}
	setmetatable(o, self)
	self.__index = self
	return o
end

function Tile:init(xpos,ypos)

	self.position = {x = xpos, y = ypos} --this is position in block coordinates, not screen coordinates

	self.left = {}
	self.right = {}
	self.above = {}
	self.below = {}

	self.canvas = love.graphics.newCanvas(48,48)

	self.state = 0 --this determines if the tile is occupied by the blob, and how much it is occupied by it. 0 for empty, 1 for filled, 2 for fortified

end

function Tile:draw()

end

function Tile:update()

end


--================================================================
TileMatrix = {}

function TileMatrix:new(o)
	local o=o or {}
	setmetatable(o, self)
	self.__index = self
	return o
end

function TileMatrix:init(topL, w, h)
	--set up initial settings
	self.topLeft = topL or {0,0}
	self.width = w or 20
	self.height = h or 14
	self.tile_size = {32,32}

	self.topLeft = {(DISPLAY_WIDTH - (self.width*self.tile_size[1]))/2, ((DISPLAY_HEIGHT - (self.height*self.tile_size[2]))/2) - 64}

	self.color = {240,240,240,220}

	self.grid_on = false

	self.matrix = {}

	--create empty tile matrix
	for j = 1, self.width do
		line = {}
		table.insert(self.matrix, line)
		for k = 1, self.height do
			local tempTile = Tile:new()
			tempTile:init(j,k)
			table.insert(line, tempTile)
		end
	end

	--now go through the matrix and set neighbours
	for _,c in ipairs(self.matrix) do
		for _,t in ipairs(c) do
			if t.position.x == 1 then
				t.left = self.matrix[self.width][t.position.y]
				t.right = self.matrix[t.position.x + 1][t.position.y]
			elseif t.position.x == self.width then
				t.left = self.matrix[t.position.x - 1][t.position.y]
				t.right = self.matrix[1][t.position.y]
			else
				t.left = self.matrix[t.position.x - 1][t.position.y]
				t.right = self.matrix[t.position.x + 1][t.position.y]
			end
			if t.position.y == 1 then
				t.above = self.matrix[t.position.x][self.height]
				t.below = self.matrix[t.position.x][t.position.y + 1]
			elseif t.position.y == self.height then
				t.above = self.matrix[t.position.x][t.position.y - 1]
				t.below = self.matrix[t.position.x][1]
			else
				t.above = self.matrix[t.position.x][t.position.y - 1]
				t.below = self.matrix[t.position.x][t.position.y + 1]
			end
		end
	end

	--local blobOrigin = {x = love.math.random(1, self.width), y = love.math.random(1, self.height)}

	--choose a random tile as the blob "seed"

	local blobOrigin = self.matrix[love.math.random(1, self.width)][love.math.random(1, self.height)]
	blobOrigin.state = 2

end

function TileMatrix:reroll()

		--now go through the matrix and set neighbours
	for _,c in ipairs(self.matrix) do
		for _,t in ipairs(c) do
			t.state = 0
		end
	end

	local blobOrigin = self.matrix[love.math.random(1, self.width)][love.math.random(1, self.height)]
	blobOrigin.state = 100
end

function TileMatrix:draw()	
	love.graphics.setFont(effectFont)
	for _,c in ipairs(self.matrix) do
		for _,t in ipairs(c) do
			if self.grid_on then
				love.graphics.setColor(WHITE)
				love.graphics.rectangle("line", (t.position.x-1)*self.tile_size[1]+self.topLeft[1], (t.position.y-1)*self.tile_size[2]+self.topLeft[2], self.tile_size[1], self.tile_size[2])
			end
			if debug_flag then
				love.graphics.setColor(AMBER)
				love.graphics.print(t.state, (t.position.x-1)*self.tile_size[1]+self.topLeft[1], (t.position.y-1)*self.tile_size[2]+self.topLeft[2])
			end
		end
	end
end

function TileMatrix:update()

end