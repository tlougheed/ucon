require 'main'

function pauseState:update(dt)
	
end

function pauseState:enter()
	musTitle:resume()
end

function pauseState:leave()
	musTitle:pause()
end

function pauseState:draw()
	love.graphics.setFont(bigFont)
	love.graphics.setColor(LIGHT_SLATE_BLUE)
	love.graphics.print('Paused', DISPLAY_WIDTH/2 - 64, DISPLAY_HEIGHT/2)		
end

function pauseState:keyreleased(key)
	if key == 'p' then
		Gamestate.switch(gameState)
	end
end