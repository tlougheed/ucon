function love.load() 	
	------------------------------------------------------- Modules --
	Camera = require 'hump.camera'
	Gamestate = require 'hump.gamestate'
	Timer = require 'hump.timer'
	Vector = require 'hump.vector'
	
	------------------------------------------------------- Static states - (these states run once in order, at start)
	bootState = {}
	splashState = {}

	------------------------------------------------------- Dynamic states - (these states interchange according to program flow 
	menuState = {}	  																	   -- note: program exits from gameState)	
	gameState = {}
	pauseState = {}
	debugState = {}
	
	-------------------------------------------------------- Register Gamestate Events
	Gamestate.registerEvents()
    
    -------------------------------------------------------- Engine files - (these files are part of the template and persist between projects)
    require 'globalconstants'
    require 'tools'
    require 'bootstate'
    --require 'splashstate'
    require 'menustate'
    require 'gamestate'
    require 'pausestate'
    --require 'debugstate'

    -------------------------------------------------------- Game object files - (these are game specific files unique to this project)
    require 'resources'
    require 'startup'
    require 'player'
    require 'tile'
    require 'blob'
    require 'missile'
    require 'lab'
    require 'wordgen'
        
    -------------------------------------------------------- Remote Debugging Support - (For ZeroBrane IDE)
    --DEBUGGER = getDebugger(arg)

    -------------------------------------------------------- Camera --
    camera = Camera()

    -------------------------------------------------------- Shaders --
    balls = {}
    for i = 1, 300 do
        table.insert(balls, {-400,-300})
    end
--     balls = {
-- {-400,-300}, {-400,-300}, {-400,-300}, {-400,-300}, {-400,-300}, {-400,-300}, {-400,-300}, {-400,-300}, {-400,-300}, {-400,-300}, {-400,-300}, {-400,-300},
-- {-400,-300}, {-400,-300}, {-400,-300}, {-400,-300}, {-400,-300}, {-400,-300}, {-400,-300}, {-400,-300},
-- {-400,-300}, {-400,-300}, {-400,-300}, {-400,-300}, {-400,-300}, {-400,-300}, {-400,-300}, {-400,-300},
-- {-400,-300}, {-400,-300}, {-400,-300}, {-400,-300}, {-400,-300}, {-400,-300}, {-400,-300}, {-400,-300},
-- {-400,-300}, {-400,-300}, {-400,-300}, {-400,-300}, {-400,-300}, {-400,-300}, {-400,-300}, {-400,-300},
-- {-400,-300}, {-400,-300}, {-400,-300}, {-400,-300}, {-400,-300}, {-400,-300}, {-400,-300}, {-400,-300}}
 
    metaballs = love.graphics.newShader(([[

        #define NBALLS %d
        extern vec2[NBALLS] balls;

        float metaball(vec2 x)
        {
            x /= 20.0;
            return 0.4 / (dot(x, x) + .00007);
        }

        vec4 effect(vec4 color, Image tex, vec2 tc, vec2 pc)
        {
            float p = 0.0;
            for (int i = 0; i < NBALLS; ++i)
                p += metaball(pc - balls[i]);
            p = ceil(p * 16.0) / 16.0;
            return vec4(p*color);
        }
    ]]):format(#balls))

    metaballs:send('balls', unpack(balls))

    -------------------------------------------------------- Game Objects --
    
	-------------------------------------------------------- Program Entry Point - (this moves out of engine initialization and into 
																				 							-- the games boot state)	
	Gamestate.switch(bootState)

end