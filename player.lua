--==========================================================================================
Player = {}

function Player:new(o)
	local o=o or {}
	setmetatable(o, self)
	self.__index = self
	return o
end

function Player:init()
	self.reload_time = 0
	self.reload_delay = 15

	self.ready = false --whether or not the player has a payload ready
end

function Player:reload()
	self.reload_time = self.reload_delay
end

function Player:update()
	if self.reload_time > 0 then
		self.reload_time = self.reload_time - 1
	end
end