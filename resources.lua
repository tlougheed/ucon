bigFont = love.graphics.newFont("assets/virgo.ttf", 36)
defaultFont = love.graphics.newFont("assets/MotionControl-Bold.otf", 28)
effectFont = love.graphics.newFont("assets/MotionControl-Bold.otf",20)--("assets/PoetsenOne-Regular.ttf", 16)
instructionFont = love.graphics.newFont("assets/MotionControl-Bold.otf", 24)
lossFont = love.graphics.newFont("assets/MotionControl-Bold.otf", 28)
menuFont = love.graphics.newFont("assets/MotionControl-Bold.otf", 28)
titleFont = love.graphics.newFont("assets/virgo.ttf", 128)
weaponFont = love.graphics.newFont("assets/MotionControl-Bold.otf", 24)
winFont = love.graphics.newFont("assets/MotionControl-Bold.otf", 36)

--spritesheets
partSS = love.graphics.newImage("assets/parts.png")

--images
mapTiled = love.graphics.newImage("assets/map_tile2.png")
mapUntiled = love.graphics.newImage("assets/map_no_tile2.png")

imgPanel = love.graphics.newImage("assets/panel.png")

imgCancel = love.graphics.newImage("assets/cancel.png")
imgCancelAlt = love.graphics.newImage("assets/cancel_pressed.png")

imgMissile = love.graphics.newImage("assets/missile.png")

--music
musTitle = love.audio.newSource("assets/spaceoompa.mp3","stream")
musTitle:setLooping(true)
musTitle:play()
musTitle:pause()

--sounds
sndAlert = love.audio.newSource("assets/alert2.wav")
sndBlobGrow = love.audio.newSource("assets/growth.wav")
sndClick = love.audio.newSource("assets/lowbeep.wav") --("assets/nullclick.wav")
sndAltClick = love.audio.newSource("assets/nullclick.wav")
sndExplode = love.audio.newSource("assets/explode.wav")
sndLaunch = love.audio.newSource("assets/launch2.wav") --needs work

--sounds for each component when it hits
sndComp = {}
sndComp[1] = love.audio.newSource("assets/explode.wav")
sndComp[2] = love.audio.newSource("assets/growth.wav")
sndComp[3] = love.audio.newSource("assets/explode.wav")
sndComp[4] = love.audio.newSource("assets/explode.wav")
sndComp[5] = love.audio.newSource("assets/explode.wav")
sndComp[6] = love.audio.newSource("assets/explode.wav")
sndComp[7] = love.audio.newSource("assets/explode.wav")
sndComp[8] = love.audio.newSource("assets/explode.wav")
sndComp[9] = love.audio.newSource("assets/explode.wav")
sndComp[10] = love.audio.newSource("assets/explode.wav")
sndComp[11] = love.audio.newSource("assets/explode.wav")
sndComp[12] = love.audio.newSource("assets/explode.wav")
sndComp[13] = love.audio.newSource("assets/explode.wav")
sndComp[14] = love.audio.newSource("assets/explode.wav")
sndComp[15] = love.audio.newSource("assets/explode.wav")
sndComp[16] = love.audio.newSource("assets/explode.wav")
sndComp[17] = love.audio.newSource("assets/explode.wav")
sndComp[18] = love.audio.newSource("assets/explode.wav")
sndComp[19] = love.audio.newSource("assets/explode.wav")
sndComp[20] = love.audio.newSource("assets/explode.wav")
sndComp[21] = love.audio.newSource("assets/explode.wav")
sndComp[22] = love.audio.newSource("assets/explode.wav")
sndComp[23] = love.audio.newSource("assets/explode.wav")
sndComp[24] = love.audio.newSource("assets/explode.wav")
sndComp[25] = love.audio.newSource("assets/explode.wav")
sndComp[26] = love.audio.newSource("assets/explode.wav")