--====================================================================================
--dragger for moving parts to the workbench
Dragger = {}

function Dragger:new(o)
	local o=o or {}
	setmetatable(o, self)
	self.__index = self
	return o
end

function Dragger:init()
	self.part = nil
	self.active = false
	self.dropping = false
	self.origin = ""
end

function Dragger:draw()
	if self.active and self.part then
		love.graphics.setColor(WHITE)
		love.graphics.draw(self.part.canvas, MOUSE.pos.x, MOUSE.pos.y) 
	end
end

function Dragger:update()

end

--=======================================================================================
--a component of the weapon
Part = {}

function Part:new(o)
	local o=o or {}
	setmetatable(o, self)
	self.__index = self
	return o
end

function Part:init(pid)
	self.ID = pid --determines the name and is used to compare with other parts
	self.effectiveness = 100 --we will assume 100% effectiveness until told otherwise
	self.times_used = 0

	self.show_effect = false --this only gets set to true if you test a device with a single component on the blob

	self.width = 64
	self.height = 64

	local quad_y = (math.ceil(self.ID/6) - 1)
	local quad_x = math.ceil(self.ID - ((quad_y)*6)) - 1
	
	self.quad = love.graphics.newQuad(quad_x*64, quad_y*64, 64, 64, 384,320)

	self.canvas = love.graphics.newCanvas(self.width, self.height)

	self.color = {love.math.random(30,230), love.math.random(30,230), love.math.random(30,230)}

	love.graphics.setCanvas(self.canvas)
		love.graphics.draw(partSS, self.quad, 0, 0)
	love.graphics.setCanvas()
end

--we won't normally draw the part using this call - rather we will just draw its canvas directly
function Part:draw(x,y)
	love.graphics.draw(self.canvas, x, y)
end


--==========================================================================================
--The toolbox contains all the parts you can use
Toolbox = {}

function Toolbox:new(o)
	local o=o or {}
	setmetatable(o, self)
	self.__index = self
	return o
end

function Toolbox:init()
	self.width = 64
	self.height = DISPLAY_HEIGHT - 16

	self.hidden = false
	self.hide_speed = 2
	self.show_speed = 2

	self.hidden_x = -56
	self.show_x = 8

	--set initial position
	self.x = 8
	self.y = 16

	self.parts = {}

	for i = 1, 26 do
		local tempPart = Part:new()
		tempPart:init(i)
		self.parts[i] = tempPart
		--print(self.parts[i].effectiveness)
	end

	local combined_effects = 0
	
	io.write("Randomizing...")

	while (combined_effects < EFFECTIVENESS_THRESHOLD) or (combined_effects > EFFECTIVENESS_THRESHOLD + 500) do
		combined_effects = 0
		for i = 1, 26 do
			local effect_type = love.math.random(1,3)
			if effect_type == 1 then
				self.parts[i].effectiveness = love.math.random(33,100)
			elseif effect_type == 2 then
				self.parts[i].effectiveness = love.math.random(-50,50)
			else
				self.parts[i].effectiveness = love.math.random(-100,-33)
			end
			combined_effects = combined_effects + self.parts[i].effectiveness
		end
	end

	print("done.")

	self.top_tool = 1
	self.tool_fit = 8
	self.tool_max = 26

	self.mouseover = nil --the part, if any, the mouse is over
	self.mouse_wait_time = 10

	self.selectedPart = nil

	self.canvas = love.graphics.newCanvas(self.width, self.height)

	love.graphics.setColor(GAINSBORO)
	love.graphics.setCanvas(self.canvas)
		for i,p in ipairs(self.parts) do
			love.graphics.setColor(self.parts[i].color)
			love.graphics.draw(partSS, self.parts[i].quad, 0, (i-1)*64)
		end
	love.graphics.setCanvas()

end


function Toolbox:draw()
	love.graphics.setColor(WHITE)
	love.graphics.draw(self.canvas, self.x, self.y)
	love.graphics.setColor(GRAY)
	love.graphics.rectangle("line", self.x, self.y, self.width, self.height)
end

function Toolbox:hide()
	self.hidden = true
end

function Toolbox:redrawCanvas()
	love.graphics.setColor(WHITE)
	self.canvas:clear()
	love.graphics.setCanvas(self.canvas)
		for i = self.top_tool, self.top_tool + self.tool_fit do
			love.graphics.setColor(self.parts[i].color)
			love.graphics.draw(partSS, self.parts[i].quad, 0, (i-self.top_tool)*64)
			if self.parts[i].show_effect or debug_flag then
				love.graphics.setFont(effectFont)
				love.graphics.setColor(GAINSBORO)
				love.graphics.print(math.ceil(self.parts[i].effectiveness), 33, (i-self.top_tool)*64 + 44)
				love.graphics.print(math.ceil(self.parts[i].effectiveness), 31, (i-self.top_tool)*64 + 46)
				love.graphics.print(math.ceil(self.parts[i].effectiveness), 33, (i-self.top_tool)*64 + 46)
				love.graphics.print(math.ceil(self.parts[i].effectiveness), 31, (i-self.top_tool)*64 + 44)
				if self.parts[i].effectiveness < 0 then
					love.graphics.setColor(RED)
				elseif self.parts[i].effectiveness < 50 then
					love.graphics.setColor(ORANGE)
				else
					love.graphics.setColor(GREEN)
				end
				love.graphics.print(math.ceil(self.parts[i].effectiveness), 32, (i-self.top_tool)*64 + 45)
			end
			if self.selectedPart then
				if self.parts[i].ID == self.selectedPart.ID then
					love.graphics.setColor(INTERNATIONAL_ORANGE)
					love.graphics.setLineWidth(4)
					love.graphics.rectangle("line", 0, (i-self.top_tool)*64, 64, 64)
					love.graphics.setLineWidth(1)
				end
			end
		end
	love.graphics.setCanvas()
end

function Toolbox:scroll(dir)
	if dir == 'up' and self.top_tool > 1 then
		self.top_tool = self.top_tool - 1
	elseif dir == 'down' and self.top_tool < self.tool_max - self.tool_fit then
		self.top_tool = self.top_tool + 1
	end

	--redraw canvas
	self:redrawCanvas()

end

function Toolbox:update()
	if self.hidden and self.x > self.hidden_x then
		self.x = self.x - self.hide_speed
	elseif self.hidden then
		self.x = self.hidden_x
	elseif not self.hidden and self.x < self.show_x then
		self.x = self.x + self.show_speed
	elseif not self.hidden then
		self.x = self.show_x
	end

	--get clicks from user
	for i = self.top_tool, self.top_tool + self.tool_fit do
		if MOUSE.pos.x >= self.x
		and MOUSE.pos.x <= self.x + 64
		and MOUSE.pos.y >= self.y + (i-self.top_tool)*64
		and MOUSE.pos.y <= self.y + (i-self.top_tool)*64 + 64
		then
			self.mouseover = self.parts[i]
			if MOUSE['l'] and double_click and self.mouse_wait_time == 0 then
				self.mouse_wait_time = 10
				self.selectedPart = self.parts[i]

				Workbench.activeSlot.part = self.selectedPart

				--redraw canvas
				self:redrawCanvas()

				--play click sound
				--sndClick:play()
				sndAltClick:play()
			end
		else
			self.mouseover = nil
		end
	end

	if self.mouse_wait_time > 0 then
		self.mouse_wait_time = self.mouse_wait_time - 1
	end

end

--==============================================================================================
--where you assemble the parts into a weapon
Workbench = {}

function Workbench:new(o)
	local o=o or {}
	setmetatable(o, self)
	self.__index = self
	return o
end

function Workbench:init()

	self.width = 320
	self.height = 128

	self.x = DISPLAY_WIDTH/2 - self.width/2
	self.y = DISPLAY_HEIGHT - 128
	
	self.parts = {}
	self.slot = {{position = {32,32}, pos = 1, part = nil}, {position = {128,32}, pos = 2, part = nil}, {position = {224,32}, pos = 3, part = nil}}
	self.slot_size = {64,64}

	self.mouseover = nil --what item, if anything, the mouse is over
	self.mouse_wait_time = 20

	self.activePart = nil
	self.activeSlot = self.slot[3]
	self.activeSlot.active = true

	self.weaponName = "F1 for help"
	self.noun = ""
	self.majAdj = ""
	self.minAdj = ""

end

function Workbench:draw()
	--draw the background panel
	love.graphics.setColor(LIGHT_SLATE_BLUE)
	--love.graphics.rectangle("fill", self.x, self.y, self.width, self.height)

	love.graphics.setColor(SILVER)
	love.graphics.draw(imgPanel, -40, -32)

	--draw the slots
	for _,s in ipairs(self.slot) do
		love.graphics.setColor(GAINSBORO)
		love.graphics.rectangle("fill", self.x + s.position[1], self.y + s.position[2], self.slot_size[1], self.slot_size[2])
		if s.part then
			love.graphics.setColor(s.part.color)
			s.part:draw(self.x + s.position[1], self.y + s.position[2])
			if s.part.show_effect or debug_flag then
				love.graphics.setFont(effectFont)
				love.graphics.setColor(GAINSBORO)
				love.graphics.print(math.ceil(s.part.effectiveness), self.x + s.position[1] + 33, self.y + s.position[2] + 44)
				love.graphics.print(math.ceil(s.part.effectiveness), self.x + s.position[1] + 31, self.y + s.position[2] + 46)
				love.graphics.print(math.ceil(s.part.effectiveness), self.x + s.position[1] + 33, self.y + s.position[2] + 46)
				love.graphics.print(math.ceil(s.part.effectiveness), self.x + s.position[1] + 31, self.y + s.position[2] + 44)
				if s.part.effectiveness < 0 then
					love.graphics.setColor(RED)
				elseif s.part.effectiveness < 50 then
					love.graphics.setColor(ORANGE)
				else
					love.graphics.setColor(GREEN)
				end
				love.graphics.print(math.ceil(s.part.effectiveness), self.x + s.position[1] + 32, self.y + s.position[2] + 45)
			end
		end
		love.graphics.setColor(DARK_GRAY)
		love.graphics.setFont(effectFont)
		love.graphics.print(s.pos, self.x + s.position[1] + 2, self.y + s.position[2])
		if s.active then
			love.graphics.setColor(LIME)
			love.graphics.setLineWidth(4)
			love.graphics.rectangle("line", self.x + s.position[1], self.y + s.position[2], self.slot_size[1], self.slot_size[2])
			love.graphics.setLineWidth(1)
		end
	end

	--draw the "Clear" buttons underneath the slots
	for _,s in ipairs(self.slot) do 
		love.graphics.setColor(WHITE)
		love.graphics.draw(imgCancel, self.x + s.position[1] + 20, self.y + s.position[2] + 70)
		--love.graphics.rectangle("fill", self.x + s.position[1] + 20, self.y + s.position[2] + 70, 24,24)
	end

	--draw the "ready to launch" text
	if self.noun ~= "" then
		
		local font_width = defaultFont:getWidth("READY TO LAUNCH")
		love.graphics.setFont(defaultFont)
		love.graphics.setColor(BLACK)
		love.graphics.print("READY TO LAUNCH", self.x + self.width/2 - font_width/2 + 1, self.y - 31)
		love.graphics.print("READY TO LAUNCH", self.x + self.width/2 - font_width/2 + 1, self.y - 33)
		love.graphics.print("READY TO LAUNCH", self.x + self.width/2 - font_width/2 - 1, self.y - 31)
		love.graphics.print("READY TO LAUNCH", self.x + self.width/2 - font_width/2 - 1, self.y - 33)
		love.graphics.setColor(RED)
		love.graphics.print("READY TO LAUNCH", self.x + self.width/2 - font_width/2, self.y - 32)
	end

	--print the name of the contraption
	
	love.graphics.setFont(weaponFont)
	local font_width = weaponFont:getWidth(self.weaponName)
	love.graphics.setColor(DARK_GRAY)
	love.graphics.print(self.weaponName, (self.x + self.width/2) - font_width/2 + 1, self.y + 6)
	love.graphics.print(self.weaponName, (self.x + self.width/2) - font_width/2 + 1, self.y + 8)
	love.graphics.print(self.weaponName, (self.x + self.width/2) - font_width/2 - 1, self.y + 6)
	love.graphics.print(self.weaponName, (self.x + self.width/2) - font_width/2 - 1, self.y + 8)
	love.graphics.setColor(WHITE)
	love.graphics.print(self.weaponName, (self.x + self.width/2) - font_width/2, self.y + 7)
	love.graphics.setFont(defaultFont)
end

function Workbench:setSlot(index)
	if self.activeSlot then
		self.activeSlot.active = false
	end
	self.activeSlot = self.slot[index]
	self.activeSlot.active = true
end

function Workbench:update()

	local part_count = 0

	for _,s in ipairs(self.slot) do
		--keep track of how many parts are loaded
		if s.part then part_count = part_count + 1 end

		if MOUSE.pos.x >= self.x + s.position[1]
		and MOUSE.pos.x <= self.x + s.position[1] + 64
		and MOUSE.pos.y >= self.y + s.position[2]
		and MOUSE.pos.y <= self.y + s.position[2] + 64
		then
			self.mouseover = s
			if MOUSE['l'] and not s.active then
				if self.activeSlot then
					self.activeSlot.active = false
				end
				s.active = true
				self.activeSlot = s
			end
		else
			self.mouseover = nil
		end

		--handle the cancel buttons
		if MOUSE.pos.x >= self.x + s.position[1] + 20
		and MOUSE.pos.x <= self.x + s.position[1] + 36
		and MOUSE.pos.y >= self.y + s.position[2] + 70
		and MOUSE.pos.y <= self.y + s.position[2] + 86
		then
			if MOUSE['l'] and self.mouse_wait_time == 0 then
				s.part = nil
				if part_count > 0 then
					part_count = part_count - 1
				end
				self.mouse_wait_time = 20
			end
		end
	end
	
	--update the name of the weapon

	--print(part_count) --DEBUG
	
	--reset all the words
	self.noun = ""
	self.majAdj = ""
	self.minAdj = ""

	if part_count == 1 then
		self.parts = {}
		for _,s in ipairs(self.slot) do

			if s.part then
				self.noun = nounList[s.part.ID]
				local capital = self.noun:sub(1, 1):upper()
				self.noun = self.noun:sub(2)
				self.noun = capital..self.noun
				
				table.insert(self.parts, s.part)
				break
			end

		end
	elseif part_count == 2 then
		self.parts = {}
		for _,s in ipairs(self.slot) do

			if s.part then
				if s.pos == 1 then
					self.majAdj = adjList[s.part.ID]
				elseif s.pos == 2 and self.slot[1].part then
					self.noun = nounList[s.part.ID]
				elseif s.pos == 2 then
					self.majAdj = adjList[s.part.ID]
				elseif s.pos == 3 then
					self.noun = nounList[s.part.ID]
				end
				table.insert(self.parts, s.part)
			end

			local capital = self.majAdj:sub(1, 1):upper()
			self.majAdj = self.majAdj:sub(2)
			self.majAdj = capital..self.majAdj

		end
	elseif part_count == 3 then
		self.parts = {self.slot[1].part,self.slot[2].part,self.slot[3].part}

		self.minAdj = minAdjList[self.slot[1].part.ID].."-"
		local capital = self.minAdj:sub(1, 1):upper()
		self.minAdj = self.minAdj:sub(2)
		self.minAdj = capital..self.minAdj
		
		self.majAdj = adjList[self.slot[2].part.ID]
		self.noun = nounList[self.slot[3].part.ID]
	end

	self.weaponName = self.minAdj..self.majAdj.." "..self.noun

	Player.ready = true
	if part_count == 0 then
		Player.ready = false
		self.weaponName = "F1 for help"
	end

	if self.mouse_wait_time > 0 then
		self.mouse_wait_time = self.mouse_wait_time - 1
	end
end