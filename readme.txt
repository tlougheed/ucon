U.C.O.N.
A game created by Ted Lougheed for Ludum Dare 32.


Installation:

Windows: Unzip the entire file and run LD32.exe.

Linux: Just run LD32.love (must have LOVE installed).


Controls:

Left-click: double-click parts from the toolbar on the left to add them to the selected slot in the workbench at the bottom of the screen. Single-click parts or slots in the workbench to select them. Click on the cancel buttons below the slots to clear them.
When at least one part is on the workbench, click on the map to launch a payload to the specified location.

Mousewheel, up/down arrows: scroll through the toolbar.

1,2,3: select corresponding workbench slot

P or spacebar: Pause game

R: Reset game

Q: exit game (must be done from pause menu or main menu)

F1: Help menu

F2: show grid overlay

F3: super secret debug/cheat mode (lets you kill the nanogoo with any weapon, no resistance). Intended for testing/evaluation purposes.