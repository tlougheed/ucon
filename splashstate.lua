local image = love.graphics.newImage("assets/splash.png")
local t = 0
local firstTime = nil

local shader = love.graphics.newShader[[
extern float time;
extern vec2 size;
extern int samples = 5; // pixels per axis; higher = bigger glow, worse performance
extern float quality = 2.5; // lower = smaller glow, better quality
 
vec4 effect(vec4 colour, Image tex, vec2 tc, vec2 sc)
{
  vec4 source = Texel(tex, tc);
  vec4 sum = vec4(0);
  int diff = (samples - 1) / 2;
  vec2 sizeFactor = vec2(1) / size * quality;
  
  for (int x = -diff; x <= diff; x++)
  {
    for (int y = -diff; y <= diff; y++)
    {
      vec2 offset = vec2(x, y) * sizeFactor;
      sum += Texel(tex, tc + offset * time * 0.5);
    }
  }
  
  return ((sum / (samples * samples)) + source) * colour;
}

]]

function splashState:update(dt)	
	t = t + dt
	if not firstTime then
		assert(image:getWidth() == DISPLAY_INFO.targetResolution.width and
			   image:getHeight() == DISPLAY_INFO.targetResolution.height, 
	   		   'Splash image must be the same size as the target resolution to work properly')
		Timer.add(1, function() Gamestate.switch(menuState) end)
		firstTime = true
	end
	
	local sinT = math.sin(t)
	shader:send("size", {200 * sinT, 200 * sinT})
	shader:send("time", t)
	Timer.update(dt)
end

function splashState:draw()
	ClearVideoBuffer()	
	
	--love.graphics.setShader(shader)
	love.graphics.draw(image, 0, 0)
	--love.graphics.setShader()		

	DrawVideoBufferToScreen()
end