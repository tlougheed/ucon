--======================================================================
Missile = {}

function Missile:new(o)
	local o=o or {}
	setmetatable(o, self)
	self.__index = self
	return o
end

function Missile:init()
	self.x = DISPLAY_WIDTH/2 + DISPLAY_WIDTH/4
	self.y = DISPLAY_HEIGHT - 4

	self.z = 100

	self.origin = {self.x, self.y}

	self.radius = 4

	self.destination = {MOUSE.pos.x - 8, MOUSE.pos.y - 8}

	self.alive = true
	self.purge = false

	self.parts = Workbench.parts

	self.explode_count = 0
	self.explode_time = love.math.random(8,12)

	self.aliveColor = WHITE
	self.explodeColor = INTERNATIONAL_ORANGE

	self.aliveImage = love.graphics.newCanvas(16,16)

	love.graphics.setColor(WHITE)
	love.graphics.setCanvas(self.aliveImage)
		love.graphics.circle("fill", 8, 8, self.radius, 8)
		--love.graphics.draw(imgMissile)
	love.graphics.setCanvas()	

	self.explodeImage = love.graphics.newCanvas(16,16)

	love.graphics.setColor(WHITE)
	love.graphics.setCanvas(self.explodeImage)
		love.graphics.circle("fill", 8, 8, 8, 16)
	love.graphics.setCanvas()	

end

function Missile:draw()
	if self.alive then
		love.graphics.setColor(self.aliveColor)
		love.graphics.draw(self.aliveImage, self.x, self.y)
		love.graphics.setColor(SILVER)
		love.graphics.line(self.x + 8, self.y + 8, self.origin[1], self.origin[2])
	else
		love.graphics.setColor(self.explodeColor)
		love.graphics.draw(self.explodeImage, self.x, self.y)
		love.graphics.setColor({192 - self.explode_count*10, 192 - self.explode_count*10, 192 - self.explode_count*10,255 - self.explode_count*30})
		love.graphics.line(self.x + 8, self.y + 8, self.origin[1], self.origin[2])
	end
end

function Missile:moveToward(coords, mag)

	local magnitude = mag or 1

	local currentPosition = {self.x, self.y}
	local distance = DistanceBetween(currentPosition, coords)
	local direction = AngleNorm(AngleBetween(coords,currentPosition))

	if distance < magnitude then
		self.x, self.y = coords[1], coords[2]
		self.alive = false
	else
		local velocity = Vector(-(math.cos(direction)*magnitude),-(math.sin(direction)*magnitude))
		self.x, self.y = self.x + velocity.x, self.y + velocity.y
	end

end

function Missile:update()
	if self.alive then
		self:moveToward(self.destination, 8)
	else
		self.explode_count = self.explode_count + 1
	end
	if self.explode_count == self.explode_time then
		local effect = 0
		for _,p in ipairs(self.parts) do
			effect = effect + p.effectiveness
		end
		effect = effect/#self.parts
		Blob:hit({self.x, self.y}, effect, self.parts)
		Toolbox:redrawCanvas()
		self.purge = true
	end
end