local getTime = love.timer.getTime
local startTime = nil
local percentComputingTimeLeftLastFrame = function()
	if startTime then		
		local millisecondsElapsed =  (getTime() - startTime) / 1000		
		local millisecondsPerFrame = (1.0 / 60.0) * 1000
		local millisecondsRemainingUntilFlip = millisecondsPerFrame - millisecondsElapsed
		startTime = getTime()
		return (millisecondsRemainingUntilFlip / millisecondsPerFrame) * 100
	else
		startTime = getTime() 
		return nil
	end
end

function gameState:enter()
	--play the "alert" sound at the beginning of the game
	sndAlert:play()
	--if music_enabled then
	musTitle:resume()
	--end
end

function gameState:leave()
	musTitle:pause()
end

function gameState:update(dt)
	FRAME_TIME_LEFT = percentComputingTimeLeftLastFrame() 		
	
	if RUNNING == nil then RUNNING = true end
	if RUNNING == false then love.event.push('quit') end

	Timer.update(dt)

	mouse_time = mouse_time + dt							

	--update shader
	t = t + dt

	-- if t % 2 < 0.1 then balls = {} end
	-- 	if #balls > 0 then

	-- 	for i = 1, #balls do
	-- 		balls[i] = {math.sin(Blob.wobble*t) + love.graphics.getWidth()/2, math.cos(Blob.wobble*t) + love.graphics.getHeight()/2}
	-- 	end

		
	-- end

	TileMatrix:update()
	if game_condition == "in progress" then 
		Blob:update()
	end

	local tempMissileArray = {}

	for _, m in ipairs(missileArray) do
		m:update()
		if not m.purge then
			table.insert(tempMissileArray, m)
		end
	end

	missileArray = tempMissileArray

	Toolbox:update()
	Workbench:update()

	Player:update()

	if Blob.coverage/300 > 90 then
		game_condition = "loss"
	elseif Blob.coverage/300 <= 0 then
		game_condition = "win"
	end

	if MOUSE['l'] and map_flag and Player.reload_time == 0 and Player.ready and game_condition == "in progress" then
		local missile = Missile:new()
		missile:init()
		table.insert(missileArray, missile)
		Player:reload()

		--play launch sound
		sndLaunch:play()
	end

end

function gameState:draw()

	Background:draw(TileMatrix.topLeft[1], TileMatrix.topLeft[2])
	--Background:draw(80, 16)
	--love.graphics.draw(mapUntiled, 80,16)

	if Blob.harm_flag then
		love.graphics.setColor({120,20,0,128})
		love.graphics.rectangle('fill', 0, 0, love.graphics.getWidth(), love.graphics.getHeight())
	end

	Blob:draw()

	love.graphics.setShader(metaballs)	
	love.graphics.rectangle('fill', 0,0,love.graphics.getWidth(), love.graphics.getHeight())
	love.graphics.setShader()

	if debug_flag then
		for _,b in ipairs(Blob.tileArray) do
			local offset = (32 - (28*(b.state/100)))/2
			love.graphics.setColor(0,180,0)
			love.graphics.rectangle("fill", (b.position.x-1)*TileMatrix.tile_size[1]+offset+TileMatrix.topLeft[1], (b.position.y-1)*TileMatrix.tile_size[2]+offset+TileMatrix.topLeft[2], 
				28*(b.state/100), 28*(b.state/100))
		end
	end

	TileMatrix:draw()

	for _, m in ipairs(missileArray) do
		m:draw()
	end

	--print(MOUSE.pos.x..", "..MOUSE.pos.y) --DEBUG

	--draw UI
	Workbench:draw()
	Toolbox:draw()

	--print HUD text
	love.graphics.setFont(defaultFont)
	love.graphics.setColor(BLACK)
	love.graphics.print("Coverage: "..tostring(math.ceil(Blob.coverage/300)).."%", 299,9)
	love.graphics.print("Coverage: "..tostring(math.ceil(Blob.coverage/300)).."%", 299,11)
	love.graphics.print("Coverage: "..tostring(math.ceil(Blob.coverage/300)).."%", 301,9)
	love.graphics.print("Coverage: "..tostring(math.ceil(Blob.coverage/300)).."%", 301,11)

	love.graphics.setColor(WHITE)
	love.graphics.print("Coverage: "..tostring(math.ceil(Blob.coverage/300)).."%", 300,10)

	-- if debug_flag then
	-- 	love.graphics.setColor(RED)
	-- 	love.graphics.print("Chance of blob growth: "..tostring(Blob.growth_chance), 300, 40)
	-- end

	if game_condition == "win" then
		local font_width = 0--winFont:getWidth("Congratulations! You have stopped the Nanogoo!")
		love.graphics.setFont(winFont)
		love.graphics.setColor(BLACK)
		love.graphics.printf("Congratulations!\
			You have stopped the Nanogoo!", 
			(DISPLAY_WIDTH/2) - 200, DISPLAY_HEIGHT/2 - DISPLAY_HEIGHT/4 + 1, DISPLAY_WIDTH/2 + 1, "center")
		love.graphics.printf("Congratulations!\
			You have stopped the Nanogoo!", 
			(DISPLAY_WIDTH/2) - 200, DISPLAY_HEIGHT/2 - DISPLAY_HEIGHT/4 + 1, DISPLAY_WIDTH/2 - 1, "center")
		love.graphics.printf("Congratulations!\
			You have stopped the Nanogoo!", 
			(DISPLAY_WIDTH/2) - 200, DISPLAY_HEIGHT/2 - DISPLAY_HEIGHT/4 - 1, DISPLAY_WIDTH/2 + 1, "center")
		love.graphics.printf("Congratulations!\
			You have stopped the Nanogoo!", 
			(DISPLAY_WIDTH/2) - 200, DISPLAY_HEIGHT/2 - DISPLAY_HEIGHT/4- 1, DISPLAY_WIDTH/2 - 1, "center")

		love.graphics.setColor(BLUE)
		love.graphics.printf("Congratulations!\
			You have stopped the Nanogoo!", 
			(DISPLAY_WIDTH/2) - 200, DISPLAY_HEIGHT/2 - DISPLAY_HEIGHT/4, DISPLAY_WIDTH/2, "center")
	elseif game_condition == "loss" then
		local font_width = lossFont:getWidth("The Nanogoo has overrun the planet!")
		love.graphics.setFont(lossFont)
		love.graphics.setColor(BLACK)
		love.graphics.print("The Nanogoo has overrun the planet!", DISPLAY_WIDTH/2 - font_width/2 + 1, DISPLAY_HEIGHT/2 - DISPLAY_HEIGHT/4 + 1)
		love.graphics.print("The Nanogoo has overrun the planet!", DISPLAY_WIDTH/2 - font_width/2 + 1, DISPLAY_HEIGHT/2 - DISPLAY_HEIGHT/4 - 1)
		love.graphics.print("The Nanogoo has overrun the planet!", DISPLAY_WIDTH/2 - font_width/2 - 1, DISPLAY_HEIGHT/2 - DISPLAY_HEIGHT/4 + 1)
		love.graphics.print("The Nanogoo has overrun the planet!", DISPLAY_WIDTH/2 - font_width/2 - 1, DISPLAY_HEIGHT/2 - DISPLAY_HEIGHT/4 - 1)
		love.graphics.setColor(RED)
		love.graphics.print("The Nanogoo has overrun the planet!", DISPLAY_WIDTH/2 - font_width/2, DISPLAY_HEIGHT/2 - DISPLAY_HEIGHT/4)
	end

	love.graphics.setColor(WHITE)

end

function gameState:keypressed(key)
	KEYS[key] = true
end

function gameState:keyreleased(key)
	KEYS[key] = false
	if key == 'p' or key == 'space' then
		Gamestate.switch(pauseState)	
	elseif key == 'f3' then
		debug_flag = not debug_flag	
	elseif key == 'escape' or key == 'f1' then
		Gamestate.switch(menuState)
	elseif key == 'r' then
		startUp()
		--play the "alert" sound at the beginning of the game
		sndAlert:play()
	elseif key == 'f2' then
		TileMatrix.grid_on = not TileMatrix.grid_on
	elseif key == 'up' then
		Toolbox:scroll('up')
	elseif key == 'down' then
		Toolbox:scroll('down')
	elseif key == '1' then
		Workbench:setSlot(1)
	elseif key == '2' then
		Workbench:setSlot(2)
	elseif key == '3' then
		Workbench:setSlot(3)
	end
end

function gameState:mousepressed(x, y, button)
	MOUSE[button] = true
	if button == 'wu' then
		Toolbox:scroll('up')
	elseif button == 'wd' then
		Toolbox:scroll('down')
	end

	if mouse_time <= double_click_interval and (button == 'l' or button == 'r') then
		double_click = true
		mouse_time = 0
	else
		double_click = false
		mouse_time = 0
	end

end

function gameState:mousereleased(x, y, button)  
	MOUSE[button] = false
end

function gameState:mousemoved(x, y, dx, dy)	
	MOUSE.delta.x = dx
	MOUSE.delta.y = dy	 
	MOUSE.pos.x, MOUSE.pos.y = x, y	

	--set the map flag if the cursor is over the map
	if MOUSE.pos.x >= TileMatrix.topLeft[1]
	and MOUSE.pos.x <= TileMatrix.topLeft[1] + TileMatrix.width*TileMatrix.tile_size[1] 
	and MOUSE.pos.y >= TileMatrix.topLeft[2]
	and MOUSE.pos.y <= TileMatrix.topLeft[2] + TileMatrix.height*TileMatrix.tile_size[2]
	then
		map_flag = true
	else
		map_flag = false
	end
end