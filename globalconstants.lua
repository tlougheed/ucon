----------------------------------------------------------------- Global variables and constants --------

----------------------- Display
DISPLAY_INFO = nil
VIDEO_BUFFER = nil 

----------------------- Input
KEYS = {}
MOUSE = {}

----------------------- Game
RUNNING = nil
FRAME_TIME_LEFT = nil
UNIQUE_ID = 0

----------------------- Logic
START_COVERAGE = 5
GROWTH_CHANCE = 4000
DIFFICULTY_FACTOR = 0.1 --the smaller this is, the easier the game is. 1 is challenging.
EFFECTIVENESS_THRESHOLD = 600 --the combined effectiveness of all parts must be greater than this before the game can start

----------------------- Debug
DEBUGGER = nil

----------------------- Colors
BLACK = {0,0,0}
WHITE = {255,255,255}
OFF_WHITE = {240,240,240}
GAINSBORO = {220,220,220}
SILVER = {192,192,192}
GRAY = {128,128,128}
DARK_GRAY = {64,64,64}
RED = {255,0,0}
MAROON = {128,0,0}
BROWN = {50,25,0}
PALE_BROWN = {150,120,80} --aka "rock"	
SUNSET = {255,128,128} --aka "mona lisa"
INTERNATIONAL_ORANGE = {255,80,0}
DARK_ORANGE = {255,128,0}
ORANGE = {255,165,0}
AMBER = {255,192,0}
YELLOW = {255,255,0}
OLIVE = {128,128,0}
LIGHT_GREEN = {128, 255, 128}
LIME = {0,255,0}
GREEN = {0,128,0}
LINCOLN_GREEN = {25,89,5}
DARK_GREEN = {0,30,50}
MALACHITE = {20,240,80}
SPRING_GREEN = {0,255,128}
DARK_TURQUOISE = {0,220,220}
AQUA = {0,255,255} 
TEAL = {0,128,128}
AZURE = {0,128,255}
NEON_BLUE = {0,148,255} --aka "dodger blue"
DEEP_SKY_BLUE = {0,160,255}
UN_BLUE = {75,146,219} --aka "summer sky"
ELECTRIC_BLUE = {125,249,255}
BLUE = {0,0,255}
NAVY = {0,0,128}
VERY_DARK_BLUE = {0,0,32}
MIDNIGHT_BLUE = {0,0,16}
MAGENTA = {255,0,255}
PURPLE = {128,0,128}
LIGHT_SLATE_BLUE = {128,128,255} --aka "tropical indigo" - seems more purple than blue
FUCHSIA_PINK = {255,128,255}