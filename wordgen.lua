--RANDOM WORD GENERATOR-----

nounList = {
	"foamer",
	"suppressor",
	"actuator",
	"fluctuator",
	"instigator",
	"mortifier",
	"metallicizer",
	"burster",
	"hydrogenator",
	"promulgator",
	"incinerator",
	"oscillator",
	"magnifier",
	"transmogrifier",
	"warper",
	"recombinator",
	"progenitor",
	"repeater",
	"laminator",
	"reticulator",
	"elevator",
	"negotiator",
	"cavitator",
	"disintegrator",
	"granulator",
	"rejuvenator"
	}

adjList = {
	"foaming",
	"suppressing",
	"actuating",
	"fluctuating",
	"instigating",
	"mortifying",
	"metallicizing",
	"bursting",
	"hydrogenating",
	"promulgating",
	"incinerating",
	"oscillating",
	"magnifying",
	"transmogrifying",
	"warping",
	"recombinating",
	"generating",
	"repeating",
	"laminating",
	"reticulating",
	"elevating",
	"negotiating",
	"cavitating",
	"disintegrating",
	"granulating",
	"rejuvenating"
}

minAdjList = {
	"quantum",
	"suppressive",
	"absorbent",
	"wave",
	"iritant",
	"stimulus",
	"metallic",
	"cycle",
	"generated",
	"radiative",
	"telescopic",
	"ordered",
	"major",
	"paired",
	"distributed",
	"combined",
	"distorted",
	"single",
	"bifurcated",
	"kinesthetic",
	"ectomorphic",
	"narcotic",
	"captive",
	"uncharged",
	"fission",
	"tactical"
}