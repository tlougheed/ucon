function debugState:update(dt)
	Timer.update(dt)
end

function debugState:draw()
	ClearVideoBuffer()

	DrawVideoBufferToScreen()
	love.graphics.print('Debug', 3, 3)
	love.graphics.print('Mouse Position : '..tostring(MOUSE.pos.x..', '..tostring(MOUSE.pos.y)), 3, 18)
	love.graphics.print('Mouse Delta   : '..tostring(MOUSE.delta.x..', '..tostring(MOUSE.delta.y)), 3, 33)	
end

function debugState:keypressed(key)
	KEYS[key] = true
end

function debugState:keyreleased(key)
	KEYS[key] = false
	if key == 'f1' then
		Gamestate.switch(gameState)
	end
end

function debugState:mousepressed(x, y, button)
	MOUSE[button] = true
end

function debugState:mousereleased(x, y, button)
	MOUSE[button] = false
	MOUSE.checkForDoubleClick()	
end

function debugState:mousemoved(x, y, dx, dy)	
	MOUSE.delta.x = dx
	MOUSE.delta.y = dy	 
	MOUSE.pos.x, MOUSE.pos.y = x, y	
end

function getDebugger(arg)
	local d = nil
	 if arg[#arg] == "-debug" then d = require("mobdebug") end
	 return d
end

function startDebugger(d)
	if d ~= nil then
		d.start()
	end
end